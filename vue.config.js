const { defineConfig } = require('@vue/cli-service')

// webpack.config.js
const AutoImport = require('unplugin-auto-import/webpack')
const Components = require('unplugin-vue-components/webpack')
const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')

module.exports = defineConfig({
    transpileDependencies: true,
    devServer: {
        open: true // 是否自动启动浏览器
    },
    configureWebpack: {
        plugins: [
            AutoImport({
                resolvers: [ElementPlusResolver()],
            }),
            Components({
                resolvers: [ElementPlusResolver()],
            }),
        ],
    },
    devServer: {
        // proxy: {
        //     '/api': {
        //         target: 'http://localhost:8081/', //接口的前缀
        //         ws: true, //代理websocked
        //         changeOrigin: true, //虚拟的站点需要更管origin
        //         pathRewrite: {
        //             '^/api': '' //重写路径
        //         }
        //     }
        // }
    },
    productionSourceMap: false
})