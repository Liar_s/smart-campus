import {ElMessage} from "element-plus"

// message消息提示
export function messageElMessage(message, type, showClose=true,) {
  ElMessage({
    message,
    type,
    showClose
  })
}