import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Login from '../views/Login'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

// 导航守卫
router.beforeEach((to, from, next) => {
  // 路径为登录，放行
  if (to.path === "/login"){
    return next()
  }
  // 获取浏览器的存储信息
  const username = window.sessionStorage.getItem("username")
  const avatarURL = window.sessionStorage.getItem("avatarURL")
  const account = window.sessionStorage.getItem("account")
  // 存储信息为空，跳转到登录页面
  if (!username && !avatarURL && !account) {
    return next("/login")
  }
  next()
})

export default router
