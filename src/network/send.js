import {request} from "@/network/request"

export function send(announcementContent) {
  return request({
    url: '/campus/setMessage',
    method: 'POST',
    header: {
      'Content-Type': 'multipart/form-data'
    },
    data: announcementContent
  })
}

export function duty(uploadData) {
  return request({
    url: '/campus/send',
    method: 'POST',
    header: {
      'Content-Type': 'multipart/form-data'
    },
    data: uploadData
  })
}


