import {request} from "@/network/request"

export function getNucleicAcidResult() {
  return request({
    url: '/campus/count',
    method: 'POST'
  })
}

