import {request} from "@/network/request"

export function getDutyInformations(time) {
  return request({
    url: '/campus/queryWork',
    method: 'POST',
    data: {
      "time": time
    }
  })
}

export function reviseDutyInformation(time) {
  return request({
    url: '/campus/range',
    method: 'GET',
    params: {
      times: time
    }
  })
}

export function saveDutyInformation(saveData) {
  return request({
    url: '/campus/range',
    method: 'GET',
    data: {
      saveData
    }
  })
}

